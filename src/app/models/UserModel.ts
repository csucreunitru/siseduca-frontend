interface UserInterface{
     //Aqui se declaran metodos y prop. relacionados a la clase
    //Prop.
    username:String;
    password :String;
    //Metodos
}

export class User implements UserInterface{
    //Aqui se implementan las prop. y los metodos
    username:String;
    password :String;
    constructor(username:String,password:String){
        this.username=username;
        this.password=password;
    }
}

