interface UbigeoInterface{
    //Aqui se declaran metodos y prop. relacionados a la clase
    //Prop.
    ubigeo :string;
    distrito:string;
    provincia:string;
    departamento:string;
    //Metodos
}

export class Ubigeo implements UbigeoInterface{
   //Aqui se implementan las prop. y los metodos
    ubigeo :string;
    distrito:string;
    provincia:string;
    departamento:string;
    constructor(ubigeo:string,departamento:string,provincia:string,distrito:string){
        this.ubigeo=ubigeo;
        this.departamento=departamento;
        this.provincia=provincia;
        this.distrito=distrito;
    }
}