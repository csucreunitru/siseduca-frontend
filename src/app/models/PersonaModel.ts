
interface PersonaInterface{
    //Aqui se declaran metodos y prop. relacionados a la clase
    //Prop.
    idpersona:number;
    nombre:string;
    apellidoPaterno :string;
    apellidoMaterno :string;
    fechaNacimiento:Date;
    edad:number;
    direccion:string;
    fechaCreacion:Date;
    fechaActualizacion:Date;
    idubigeo:number;
    estado:number;
    documento:string;
    //Metodos
    
}

export class Persona implements PersonaInterface{
    //Aqui se implementan las prop. y los metodos
    idpersona:number;
    nombre:string;
    apellidoPaterno :string;
    apellidoMaterno :string;
    fechaNacimiento:Date;
    edad:number;
    direccion:string;
    fechaCreacion:Date;
    fechaActualizacion:Date;
    idubigeo:number;
    estado:number;
    documento:string;

    username:string;
    passsword:string;
    
    constructor(idpersona:number,nombre:string,apellidoPaterno:string,apellidoMaterno:string,
                fechaNacimiento:Date,edad:number,direccion="S/D",idubigeo:number,estado=0,documento:string,
                fechaCreacion=new Date(),fechaActualizacion?:Date)
    {
        this.idpersona=idpersona;
        this.nombre=nombre;
        this.apellidoPaterno=apellidoPaterno;
        this.apellidoMaterno=apellidoMaterno;
        this.fechaNacimiento=fechaNacimiento;
        this.edad=edad;
        this.direccion=direccion;
        this.idubigeo=idubigeo;
        this.estado=estado;
        this.documento=documento;
        this.fechaCreacion=fechaCreacion;
        this.fechaActualizacion=fechaActualizacion;            
    }
}