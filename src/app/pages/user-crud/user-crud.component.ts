import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { UserService }from'../../services/user.service';
import { FormControl, FormGroup } from '@angular/forms';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import {DateDiff} from 'date-diff';
import Swal from 'sweetalert2';

import { Persona } from "../../models/PersonaModel";
import { Ubigeo } from 'src/app/models/UbigeoModel';

@Component({
  selector: 'app-user-crud',
  templateUrl: './user-crud.component.html',
  styleUrls: ['./user-crud.component.css']
})
export class UserCRUDComponent implements OnInit {
  
  @BlockUI() blockUI: NgBlockUI;
  olPersonas:Array<Persona>=[];
  oPersona:Persona;
  frmUsuario:FormGroup;

  constructor(private modalService: NgbModal,private _userService:UserService ) { 
    this.listarUsuarios();
  }

  ngOnInit(): void {
  }

  closeResult = '';

  open(content) {
    this.initFrmUsuario();
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',centered: true}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      if (result==='Save'){
        this.registrarUsuario();
      }
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openEdit(content,idpersona) {
    this.initEditFrmUsuario(idpersona);
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',centered: true}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      if (result==='Save'){
        this.editarUsuarios(idpersona);
      }
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  private initFrmUsuario(){
    this.frmUsuario = new FormGroup({
      nombre: new FormControl(''),
      apellidoPaterno: new FormControl(''),
      apellidoMaterno: new FormControl(''),
      fechaNacimiento: new FormControl(''),
      direccion: new FormControl(''),
      usuario: new FormControl(''),
      password: new FormControl(''),
      passwordConfirm: new FormControl(''),
      iddepartamento: new FormControl(''),
      idprovincia: new FormControl(''),
      iddistrito: new FormControl(''),
    });

  }

  private initEditFrmUsuario(idpersona){
    let oPersona=this.olPersonas.find(element=>element.idpersona==idpersona);
    this.frmUsuario = new FormGroup({
      nombre: new FormControl(oPersona.nombre),
      apellidoPaterno: new FormControl(oPersona.apellidoPaterno),
      apellidoMaterno: new FormControl(oPersona.apellidoMaterno),
      fechaNacimiento: new FormControl(oPersona.fechaNacimiento),
      direccion: new FormControl(oPersona.direccion),
      usuario: new FormControl(oPersona.username),
      password: new FormControl(oPersona.passsword),
      passwordConfirm: new FormControl(''),
      iddepartamento: new FormControl(''),
      idprovincia: new FormControl(''),
      iddistrito: new FormControl(''),
    });

  }

  private registrarUsuario(){
    this.blockUI.start('Registrando usuario...');
    let frm=this.frmUsuario.value;
    let fechaActual=new Date();
    let fechaNacimiento=new Date(frm.fechaNacimiento);
    let edad=fechaActual.getFullYear()-fechaNacimiento.getFullYear();
    let oPersona = new Persona(1,frm.nombre,frm.apellidoPaterno,frm.apellidoMaterno,
                            frm.fechaNacimiento,edad,frm.direccion,1,1,'');  
    oPersona.username=frm.usuario;
    oPersona.passsword=frm.password;
    this._userService.setUsuarios(oPersona).subscribe(
      result => {
        console.log(result);
        if(result==='Creado con exito'){
          this.listarUsuarios();
          Swal.fire({
            icon: 'success',
            title: 'Usuario registrado',
            showClass: {
              popup: 'animate__animated animate__fadeInDown'
            },
            hideClass: {
              popup: 'animate__animated animate__fadeOutUp'
            },
            showConfirmButton: false,
            timer: 1000
            });
        }        
        this.blockUI.stop();
        },
      error => {
        console.log(<any>error);
        this.blockUI.stop();
      }
    );
  }

  private listarUsuarios(){
    this.olPersonas=[];
    this._userService.getPersonas().subscribe(
      result => {
        this.blockUI.start('Cargando usuarios...');
        result.forEach(element => {
          let oPersona=new Persona(element.persona_id,element.nombres,element.a_paterno,element.a_materno,
                                  new Date(),element.edad,element.direccion,element.ubigeo_id,element.estado,
                                  element.dni);
          this.olPersonas.push(oPersona);
        });
        this.blockUI.stop();
        Swal.fire({
          icon: 'success',
          title: 'Usuarios encontrados',
          showClass: {
            popup: 'animate__animated animate__fadeInDown'
          },
          hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
          },
          showConfirmButton: false,
          timer: 1000
        })
      },
      error => {
          console.log(<any>error);
          this.blockUI.stop();
      }
    );
  }

  private editarUsuarios(idpersona){
    console.log('Actualizando');
    this.blockUI.start('Actualizando usuario...');
    let frm=this.frmUsuario.value;
    let fechaActual=new Date();
    let fechaNacimiento=new Date(frm.fechaNacimiento);
    let edad=fechaActual.getFullYear()-fechaNacimiento.getFullYear();
    let oPersona = new Persona(idpersona,frm.nombre,frm.apellidoPaterno,frm.apellidoMaterno,
                            frm.fechaNacimiento,edad,frm.direccion,1,1,'');  
    oPersona.username=frm.usuario;
    oPersona.passsword=frm.password;
    this._userService.editUsuarios(oPersona).subscribe(
      result => {
        console.log(result);
        if(result==='Actualizado con éxito'){
          this.listarUsuarios();
          Swal.fire({
            icon: 'success',
            title: 'Usuario actualizado',
            showClass: {
              popup: 'animate__animated animate__fadeInDown'
            },
            hideClass: {
              popup: 'animate__animated animate__fadeOutUp'
            },
            showConfirmButton: false,
            timer: 1000
            });
        }        
        this.blockUI.stop();
        },
      error => {
        console.log(<any>error);
        this.blockUI.stop();
      }
    );
  }

  private eliminarUsuarios(idpersona){
    let oPersona=this.olPersonas.find(element=>element.idpersona==idpersona);
    let swalDelete = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    swalDelete.fire({
      title: `¿Seguro de eliminar al usuario?`,
      text: `Usuario: ${oPersona.nombre} ${oPersona.apellidoPaterno} ${oPersona.apellidoMaterno}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar usuario',
      cancelButtonText: 'No, cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        console.log('Eliminando');
        this._userService.deleteUsuarios(oPersona).subscribe(
          result => {
            console.log(result);
            if(result==='Eliminado con éxito'){
              this.listarUsuarios();
              Swal.fire({
                icon: 'success',
                title: 'Usuario removido',
                showClass: {
                  popup: 'animate__animated animate__fadeInDown'
                },
                hideClass: {
                  popup: 'animate__animated animate__fadeOutUp'
                },
                showConfirmButton: false,
                timer: 1000
                });
            }else{
              swalDelete.fire(
                'Error',
                result,
                'error'
              )
            }        
            this.blockUI.stop();
            },
          error => {
            swalDelete.fire(
              'Error',
              'No se eliminó al usuario',
              'error'
            )
            console.log(<any>error);
            this.blockUI.stop();
          }
        );
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalDelete.fire(
          'Cancelado',
          'No se eliminó al usuario',
          'error'
        )
      }
    })
  }
}
