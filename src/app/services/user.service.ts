import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";
import {map} from 'rxjs/operators';
import { Persona } from '../models/PersonaModel';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  pathApi="http://localhost:5000/siseduca-webapi/us-central1/app/";
  pathPersona="persona/";
  constructor(public http: HttpClient) { }

  getPersonas(): any{
    let url=this.pathApi+this.pathPersona;
    let options={headers:new HttpHeaders({'Content-Type': 'application/json'})};
    return this.http.post(`${url}listar_personas`,null,options);
  }

  setUsuarios(oPersona:Persona): any{
    let param={
      login:oPersona.username,
      pass:oPersona.passsword,
      nombres:oPersona.nombre,
      a_paterno:oPersona.apellidoPaterno,
      a_materno:oPersona.apellidoMaterno,
      dni:oPersona.documento,
      fecha:oPersona.fechaNacimiento,
      edad:oPersona.edad,
      direccion:oPersona.direccion,
      ubigeo_id:1
    };
    let url=this.pathApi+this.pathPersona;
    let options={headers:new HttpHeaders({'Content-Type': 'application/json'})};
    return this.http.post(`${url}registrar_persona`,param,options);
  }

  editUsuarios(oPersona:Persona): any{
    let param={
      persona_id:oPersona.idpersona,
      nombres:oPersona.nombre,
      a_paterno:oPersona.apellidoPaterno,
      a_materno:oPersona.apellidoMaterno,
      dni:oPersona.documento,
      fecha:oPersona.fechaNacimiento,
      edad:oPersona.edad,
      direccion:oPersona.direccion,
      ubigeo_id:1
    };
    let url=this.pathApi+this.pathPersona;
    let options={headers:new HttpHeaders({'Content-Type': 'application/json'})};
    return this.http.post(`${url}actualizar_persona`,param,options);
  }

  deleteUsuarios(idPersona): any{
    let param={
      persona_id:idPersona
    };
    let url=this.pathApi+this.pathPersona;
    let options={headers:new HttpHeaders({'Content-Type': 'application/json'})};
    return this.http.post(`${url}eliminar_persona`,param,options);
  }

}
